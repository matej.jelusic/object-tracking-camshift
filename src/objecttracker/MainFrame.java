package objecttracker;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;

import org.opencv.core.Core;
import org.opencv.core.Point;

public class MainFrame extends JFrame{
	static JLabel videoScreen = new JLabel();
	static JLabel backProjScreen = new JLabel();
	
	static FrameProcessor processor;
	JButton start;
	JButton stop;
	PlayThread playThread;
	static ExecutorService executor = Executors.newFixedThreadPool(1);
	
	static {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}
	
	public MainFrame() {
		setTitle("Object tracker");
		this.addWindowListener(new WindowAdapter() {
			   public void windowClosing(WindowEvent we) {
				  executor.shutdownNow();
				  
				  while (!executor.isTerminated()) {
				  }


				  //dispose();
				  dispose();
			      System.exit(1);
			   }
		});
		initGUI();
		
		playThread = new PlayThread();
	}
	
	
	private void initGUI() {
		setBounds(new Rectangle(500,500,500,500));
		
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		
		videoScreen.setHorizontalTextPosition(JLabel.CENTER);
		
		
		start = new JButton("Start");
		stop = new JButton("Stop");
		
		videoScreen.addMouseListener(new MouseListener() {
			
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				if(processor.isStop()){
					if(processor.points.size() != 4){
						processor.points.add(new Point(e.getX(), e.getY()));
						Graphics g = videoScreen.getGraphics();
						g.setColor(Color.GREEN);
						
						
						g.drawOval(e.getX()-5, e.getY()-5, 10, 10);
					}
				}
				
			}
		});
		
		start.addActionListener((a) -> {
			start.setEnabled(false);
			executor.execute(new PlayThread());
			
			stop.setEnabled(true);
		});
		stop.addActionListener((a) -> {
			processor.stop();
			
			stop.setEnabled(false);
			start.setEnabled(true);
		});
		start.setEnabled(false);
		stop.setEnabled(false);
		
		JPanel controls = new JPanel();
		controls.setLayout(new FlowLayout());
		controls.add(start);
		controls.add(stop);
		
		JToolBar toolbar = new JToolBar();
		
		JButton openWebCamButton = new JButton("Open web cam");
		openWebCamButton.addActionListener((a)->{
			start.setEnabled(true);
			processor.openWebCam();
		});
		
		toolbar.add(openWebCamButton);
		JPanel screens = new JPanel();
		screens.setLayout(new GridLayout(1,1));
		screens.add(videoScreen);
		screens.add(backProjScreen);
		
		cp.add(toolbar, BorderLayout.NORTH);
		cp.add(screens, BorderLayout.CENTER);
		cp.add(controls, BorderLayout.SOUTH);
		
		
	}


	public static void main(String[] args) {
		SwingUtilities.invokeLater(()->{
			new MainFrame().setVisible(true);
		});
		processor = new FrameProcessor(videoScreen, backProjScreen);	
	}

	private static class PlayThread extends Thread{
		@Override
		public void run() {
			processor.play();
			
		}
	}
	
	
}